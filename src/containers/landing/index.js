// imports
import React from 'react';

// components
import Lockup from '../../components/lockup';

// landing page
const Landing = () => (
	<div>
		<Lockup links />
	</div>
);


// export
export default Landing
