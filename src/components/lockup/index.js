// imports
import React from 'react'

// styles
import './styles.css'

// landing page
const Lockup = ({ links }) => (
	<div>
		<div className="LockupContainer">
			<h1 className="mainTitle">&nbsp;&nbsp;&nbsp;GONG</h1>
			<div className="xContainer">
				<span className="line c" />
				<span className="line d" />
				<span className="line a" />
				<span className="line b" />
				<span className="year a">2</span>
				<span className="year b">0</span>
				<span className="year c">1</span>
				<span className="year d">8</span>
				<h4 className="desciption">‘Work&nbsp;the&nbsp;work’</h4>
			</div>
			<h1>CROWBAR</h1>
		</div>

		{ links && 
			<React.Fragment>
				<a className="innerLinks left">{'<'} To the Gong</a>
				<a className="innerLinks right">To the Crowbar {'>'}</a>
			</React.Fragment>
		}
	</div>
)

// export
export default Lockup
