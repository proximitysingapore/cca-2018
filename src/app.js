// imports
import React from 'react';
import { Route, Link } from 'react-router-dom';
// pages
import Home from './containers/home';
import About from './containers/about';
import Landing from './containers/landing';

const App = () => (
  <React.Fragment>
    <Route exact path="/" component={Landing} />
    <Route exact path="/about-us" component={About} />
  </React.Fragment>
)

export default App
