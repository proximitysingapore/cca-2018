import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import { ConnectedRouter } from 'connected-react-router'
import store, { history } from './store'
import App from './app.js'

import 'normalize.css';
import './index.css'

render(
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <div className="rootElement">
        <App />
      </div>
    </ConnectedRouter>
  </Provider>,
  document.querySelector('#root')
)
